#!/usr/bin/env bash

docker run --rm -t -v $(pwd)/build:/usr/src/build:z -v $(pwd)/prebuild:/usr/src/prebuild:z -v $(pwd)/build.sh:/usr/src/build.sh:z fedora:$1 /usr/src/build.sh

