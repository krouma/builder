#!/usr/bin/env bash

if [ -f /usr/src/nvidia-driver/nvidia-kmod-${PACKAGE_VERSION}-x86_64.tar.xz ]; then
    ln -s /usr/src/nvidia-driver/nvidia-kmod-${PACKAGE_VERSION}-x86_64.tar.xz .
else
    source /usr/src/nvidia-driver/nvidia-generate-tarballs.sh
fi

