#!/usr/bin/env bash

function install_package() {
    IFS=';' read -r -a PACKAGE <<< $1
    PACKAGE_NAME=${PACKAGE[0]}
    PACKAGE_URL=${PACKAGE[1]}
    PACKAGE_ARCHS=${PACKAGE[2]}
    
    echo "========================================================================"
    echo "Preparing package ${PACKAGE_NAME} in version ${PACKAGE_FULL_VERSION}"
    echo "========================================================================"
    
    git clone ${PACKAGE_URL} /usr/src/${PACKAGE_NAME}
    cd /usr/src/${PACKAGE_NAME}
    
    PACKAGE_VERSION=$(cat ${PACKAGE_NAME}.spec | grep Version | cut -d ' ' -f 9)
    PACKAGE_RELEASE=$(cat ${PACKAGE_NAME}.spec | grep Release | cut -d ' ' -f 9 | cut -d '%' -f1)
    PACKAGE_FULL_VERSION=${PACKAGE_VERSION}-${PACKAGE_RELEASE}
    
    if [ -d /usr/src/build/${FEDORA_RELEASE}/${PACKAGE_NAME}/${PACKAGE_FULL_VERSION} ]; then
        echo "========================================================================"
        echo "Package ${PACKAGE_NAME} in version ${PACKAGE_FULL_VERSION} is already built"
        echo "========================================================================"
    else
        echo "========================================================================"
        echo "Building package ${PACKAGE_NAME} in version ${PACKAGE_FULL_VERSION}"
        echo "========================================================================"
        
        # Prebuild
        cd /usr/src/${PACKAGE_NAME}
        if [ -f "/usr/src/prebuild/${PACKAGE_NAME}.sh" ]; then
            source /usr/src/prebuild/${PACKAGE_NAME}.sh
        fi
        mkdir -p /usr/src/build/${FEDORA_RELEASE}/${PACKAGE_NAME}/${PACKAGE_FULL_VERSION}
        dnf builddep -y ${PACKAGE_NAME}.spec
        
        # Build
        fedpkg --release ${RELEASE} local --arch $PACKAGE_ARCHS
        
        # Postbuild
        for DIR in x86_64 i686 noarch; do
            if [ -d $DIR ]; then
                cp -r $DIR /usr/src/build/${FEDORA_RELEASE}/${PACKAGE_NAME}/${PACKAGE_FULL_VERSION}/
            fi
        done
    fi  
}

dnf install -y git wget xz fedora-packager make

FEDORA_VERSION=$(cat /etc/os-release | grep VERSION_ID | cut -d '=' -f 2)
RELEASE=f${FEDORA_VERSION}
FEDORA_RELEASE=fc${FEDORA_VERSION}

declare -a PACKAGES
PACKAGES+=("nvidia-driver;https://github.com/negativo17/nvidia-driver.git;x86_64,i686")
PACKAGES+=("dkms-nvidia;https://github.com/negativo17/dkms-nvidia.git;x86_64")
PACKAGES+=("nvidia-settings;https://github.com/negativo17/nvidia-settings.git;x86_64")
PACKAGES+=("nvidia-kmod-common;https://github.com/negativo17/nvidia-kmod-common.git;noarch")

for PACKAGE in ${PACKAGES[@]}; do
    install_package $PACKAGE
done

