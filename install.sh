#!/usr/bin/env bash

VERSION=$(ls -1 build/ | tail -1)
RELEASE="1.fc$(cat /etc/os-release | grep VERSION_ID | cut -d '=' -f 2)"


for PACKAGE in nvidia-driver nvidia-driver-libs dkms-nvidia nvidia-settings nvidia-libXNVCtrl nvidia-driver-NVML; do
    PACKAGES="${PACKAGES} build/${VERSION}/x86_64/${PACKAGE}-${VERSION}-${RELEASE}.x86_64.rpm"
done

for PACKAGE in nvidia-driver-libs; do
    PACKAGES="${PACKAGES} build/${VERSION}/i686/${PACKAGE}-${VERSION}-${RELEASE}.i686.rpm"
done

for PACKAGE in nvidia-kmod-common; do
    PACKAGES="${PACKAGES} build/${VERSION}/noarch/${PACKAGE}-${VERSION}-${RELEASE}.noarch.rpm"
done

dnf install -y --allowerasing ${PACKAGES}
